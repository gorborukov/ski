class AddTopToSights < ActiveRecord::Migration[5.0]
  def change
    add_column :sights, :top, :boolean
  end
end
