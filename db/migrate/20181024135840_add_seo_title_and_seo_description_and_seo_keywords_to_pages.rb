class AddSeoTitleAndSeoDescriptionAndSeoKeywordsToPages < ActiveRecord::Migration[5.0]
  def change
    add_column :pages, :seo_title, :string
    add_column :pages, :seo_description, :string
    add_column :pages, :seo_keywords, :string
  end
end
