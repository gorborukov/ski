class AddCoordinatesToHotel < ActiveRecord::Migration[5.0]
  def change
    add_column :hotels, :coordinates, :string
  end
end
