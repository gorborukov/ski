class AddAttachmentPictureToSights < ActiveRecord::Migration[5.0]
  def self.up
    change_table :sights do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :sights, :picture
  end
end
