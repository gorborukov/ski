class AddExternalToHotel < ActiveRecord::Migration[5.0]
  def change
  	add_column :hotels, :external, :jsonb, null: false, default: '{}'
    add_index  :hotels, :external, using: :gin
  end
end
