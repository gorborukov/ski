class AddBookingScriptToHotel < ActiveRecord::Migration[5.0]
  def change
    add_column :hotels, :booking_script, :string
  end
end
