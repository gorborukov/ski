class AddPriceAndRatingToHotel < ActiveRecord::Migration[5.0]
  def change
    add_column :hotels, :price, :integer
    add_column :hotels, :rating, :float
  end
end
