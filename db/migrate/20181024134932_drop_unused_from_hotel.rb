class DropUnusedFromHotel < ActiveRecord::Migration[5.0]
  def change
  	remove_column :hotels, :excerpt
  	remove_column :hotels, :content
  	remove_column :hotels, :features
  	remove_column :hotels, :photos
  end
end
