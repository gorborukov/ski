class AddSeoTitleAndSeoDescriptionAndSeoKeywordsToSights < ActiveRecord::Migration[5.0]
  def change
    add_column :sights, :seo_title, :string
    add_column :sights, :seo_description, :string
    add_column :sights, :seo_keywords, :string
  end
end
