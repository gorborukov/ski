class CreateHotels < ActiveRecord::Migration[5.0]
  def change
    create_table :hotels do |t|
      t.string :name
      t.string :excerpt
      t.text :content
      t.string :data_url
      t.integer :stars
      t.text :features
      t.text :photos
      t.text :rooms
      t.text :services
      t.text :reviews

      t.timestamps
    end
  end
end
