class CreateSights < ActiveRecord::Migration[5.0]
  def change
    create_table :sights do |t|
      t.string :name
      t.string :excerpt
      t.text :content
      t.string :coordinates

      t.timestamps
    end
  end
end
