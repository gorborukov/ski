class AddTopToHotels < ActiveRecord::Migration[5.0]
  def change
    add_column :hotels, :top, :boolean
  end
end
