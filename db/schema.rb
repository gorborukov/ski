# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181024210021) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "hotels", force: :cascade do |t|
    t.string   "name"
    t.string   "data_url"
    t.integer  "stars"
    t.text     "rooms"
    t.text     "services"
    t.text     "reviews"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.jsonb    "external",             default: "{}", null: false
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.bigint   "picture_file_size"
    t.datetime "picture_updated_at"
    t.integer  "price"
    t.float    "rating"
    t.string   "booking_script"
    t.string   "coordinates"
    t.boolean  "top"
    t.index ["external"], name: "index_hotels_on_external", using: :gin
  end

  create_table "pages", force: :cascade do |t|
    t.string   "name"
    t.string   "excerpt"
    t.text     "content"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "seo_title"
    t.string   "seo_description"
    t.string   "seo_keywords"
  end

  create_table "sights", force: :cascade do |t|
    t.string   "name"
    t.string   "excerpt"
    t.text     "content"
    t.string   "coordinates"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.string   "seo_title"
    t.string   "seo_description"
    t.string   "seo_keywords"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.bigint   "picture_file_size"
    t.datetime "picture_updated_at"
    t.boolean  "top"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

end
