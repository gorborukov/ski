lock "~> 3.11.0"
set :application, "ski"
set :repo_url, "git@bitbucket.org:gorborukov/ski.git"
set :deploy_to, "/home/cloudtravel/ski"
append :linked_files, "config/database.yml", "config/secrets.yml"
append :linked_dirs, "log", "tmp/pids", "tmp/cache", "tmp/sockets", "public/system"
set :passenger_restart_with_touch, true
