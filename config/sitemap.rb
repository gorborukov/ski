# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://kraspol.online"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  add hotels_path, :priority => 0.7, :changefreq => 'monthly'
  add sights_path, :priority => 0.7, :changefreq => 'monthly'
  add pages_path, :priority => 0.7, :changefreq => 'monthly'
  add map_path, :priority => 0.7, :changefreq => 'monthly'
  #
  # Add all articles:
  #
  Hotel.find_each do |hotel|
    add hotel_path(hotel), :lastmod => hotel.updated_at
  end

  Sight.find_each do |sight|
    add sight_path(sight), :lastmod => sight.updated_at
  end

  Page.find_each do |page|
    add page_path(page), :lastmod => page.updated_at
  end
end
