Rails.application.routes.draw do
  devise_for :users
  resources :pages
  resources :hotels
  resources :sights
  root 'pages#home'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get 'map' => 'pages#map', as: 'map'
end
