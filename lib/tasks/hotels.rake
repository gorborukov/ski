require 'csv'
namespace :hotels do
  desc "Populate hotels from .csv file"
  task :populate => :environment do
	CSV.open('./public/hotels.csv', 'r').each do |row|
	  Hotel.create(name: row[0], data_url: row[1], picture_remote_url: row[2])
	end  
  end

  desc "Update rating, price and stars from API data"
  task :update => :environment do
	Hotel.all.each do |hotel|
      hotel.rating = hotel.external['table']['rating'].to_f rescue 0
	  hotel.stars = hotel.external['table']['stars'].to_i rescue 0
	  hotel.price = hotel.external['table']['price'].to_i rescue 0
	  hotel.save
	end
  end	
end