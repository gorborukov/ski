class SightsController < ApplicationController
  before_action :set_sight, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :new, :update, :destroy, :create]

  # GET /sights
  # GET /sights.json
  def index
    set_meta_tags title: 'Достопримечательности и интересные места Красной Поляны',
              description: 'Информация о достопримечательностях Красной Поляны: карты, адреса, путеводители',
              keywords: 'Красная Поляна, достопримечательности, карта, как добраться'
    @sights = Sight.all
    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :index }
    end
  end

  # GET /sights/1
  # GET /sights/1.json
  def show
    set_meta_tags title: @sight.seo_title.present? ? @sight.seo_title : @sight.name,
              description: @sight.seo_description.present? ? @sight.seo_description : @sight.excerpt,
              keywords: @sight.seo_keywords.present? ? @sight.seo_keywords : @sight.name + ', Красная Поляна, как добраться'
  end

  # GET /sights/new
  def new
    @sight = Sight.new
  end

  # GET /sights/1/edit
  def edit
  end

  # POST /sights
  # POST /sights.json
  def create
    @sight = Sight.new(sight_params)

    respond_to do |format|
      if @sight.save
        format.html { redirect_to @sight, notice: 'Sight was successfully created.' }
        format.json { render :show, status: :created, location: @sight }
      else
        format.html { render :new }
        format.json { render json: @sight.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /sights/1
  # PATCH/PUT /sights/1.json
  def update
    respond_to do |format|
      if @sight.update(sight_params)
        format.html { redirect_to @sight, notice: 'Sight was successfully updated.' }
        format.json { render :show, status: :ok, location: @sight }
      else
        format.html { render :edit }
        format.json { render json: @sight.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /sights/1
  # DELETE /sights/1.json
  def destroy
    @sight.destroy
    respond_to do |format|
      format.html { redirect_to sights_url, notice: 'Sight was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sight
      @sight = Sight.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sight_params
      params.require(:sight).permit(:picture, :name, :excerpt, :content, :coordinates, :top, :seo_title, :seo_description, :seo_keywords)
    end
end
