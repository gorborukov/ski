class HotelsController < ApplicationController
  before_action :set_hotel, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :new, :update, :destroy, :create]

  # GET /hotels
  # GET /hotels.json
  def index
    set_meta_tags title: 'Отели и гостиницы Красной Поляны',
              description: 'Лучшие отели и гостиницы Красной Поляны. Цены, фотографии, номера и бронирование.',
              keywords: 'Красная Поляна, отели, гостиницы, бронирование'
    @q = Hotel.ransack(params[:q])
    @hotels = @q.result(distinct: true).page(params[:page]).order('name ASC')
    @all_hotels = Hotel.all
    data = Hotel.all.as_json(only: [:id, :name, :coordinates], root: false) 
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: data }
      format.xml { render :index }
    end
  end

  # GET /hotels/1
  # GET /hotels/1.json
  def show
    set_meta_tags title: @hotel.name,
              description: @hotel.external['table']['description'],
              keywords: @hotel.name + ', отель, бронирование, фото, цены, номера, отзывы, Красная Поляна'
  end

  # GET /hotels/new
  def new
    @hotel = Hotel.new
  end

  # GET /hotels/1/edit
  def edit
  end

  # POST /hotels
  # POST /hotels.json
  def create
    @hotel = Hotel.new(hotel_params)

    respond_to do |format|
      if @hotel.save
        format.html { redirect_to @hotel, notice: 'Hotel was successfully created.' }
        format.json { render :show, status: :created, location: @hotel }
      else
        format.html { render :new }
        format.json { render json: @hotel.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /hotels/1
  # PATCH/PUT /hotels/1.json
  def update
    respond_to do |format|
      if @hotel.update(hotel_params)
        format.html { redirect_to @hotel, notice: 'Hotel was successfully updated.' }
        format.json { render :show, status: :ok, location: @hotel }
      else
        format.html { render :edit }
        format.json { render json: @hotel.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /hotels/1
  # DELETE /hotels/1.json
  def destroy
    @hotel.destroy
    respond_to do |format|
      format.html { redirect_to hotels_url, notice: 'Hotel was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_hotel
      @hotel = Hotel.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def hotel_params
      params.require(:hotel).permit(:picture, :name, :data_url, :stars, :price, :rating, :rooms, :services, :reviews, :booking_script, :top)
    end
end
