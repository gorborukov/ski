class PagesController < ApplicationController
  before_action :set_page, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:edit, :new, :update, :destroy, :create]

  def map
    set_meta_tags title: 'Отели Красной Поляны на карте',
              description: 'Карта отелей Красной Поляны, поиск отеля на карте',
              keywords: 'Красная Поляна, карта, отели'
    @hotels = Hotel.all
    respond_to do |format|
      format.html # show.html.erb
      format.xml { render :index }
    end
  end

  def home
    set_meta_tags title: 'Отели и достопримечательности Красной Поляны',
              description: 'Отдых на Красной Поляне, информация о лучших отелях и достопримечательностях',
              keywords: 'Красная Поляна, отели, что посмотреть, как добраться, достопримечательности, отзывы'
  end
  # GET /pages
  # GET /pages.json
  def index
    set_meta_tags title: 'Полезные статьи о Красной Поляне',
              description: 'Статьи на разные темы о Красной Поляне, выбор отеля, как добраться и многое другое',
              keywords: 'Красная Поляна, статьи, выбор отеля, как добраться'
    @pages = Page.all
  end

  # GET /pages/1
  # GET /pages/1.json
  def show
    set_meta_tags title: @page.seo_title.present? ? @page.seo_title : @page.name,
              description: @page.seo_description.present? ? @page.seo_description : @page.excerpt,
              keywords: @page.seo_keywords.present? ? @page.seo_keywords : @page.name + ', Красная Поляна'
  end

  # GET /pages/new
  def new
    @page = Page.new
  end

  # GET /pages/1/edit
  def edit
  end

  # POST /pages
  # POST /pages.json
  def create
    @page = Page.new(page_params)

    respond_to do |format|
      if @page.save
        format.html { redirect_to @page, notice: 'Page was successfully created.' }
        format.json { render :show, status: :created, location: @page }
      else
        format.html { render :new }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /pages/1
  # PATCH/PUT /pages/1.json
  def update
    respond_to do |format|
      if @page.update(page_params)
        format.html { redirect_to @page, notice: 'Page was successfully updated.' }
        format.json { render :show, status: :ok, location: @page }
      else
        format.html { render :edit }
        format.json { render json: @page.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.json
  def destroy
    @page.destroy
    respond_to do |format|
      format.html { redirect_to pages_url, notice: 'Page was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_page
      @page = Page.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def page_params
      params.require(:page).permit(:name, :excerpt, :content, :seo_title, :seo_description, :seo_keywords)
    end
end
