json.extract! sight, :id, :name, :excerpt, :content, :coordinates, :created_at, :updated_at
json.url sight_url(sight, format: :json)
