json.extract! hotel, :id, :name, :excerpt, :content, :data_url, :stars, :features, :photos, :rooms, :services, :reviews, :created_at, :updated_at
json.url hotel_url(hotel, format: :json)
