import React from "react"
import PropTypes from "prop-types"
import ImageGallery from 'react-image-gallery'

class PageGallery extends React.Component {
  constructor(props) {
      super(props);
	  this.state = {
		photos: props.photos
	  }
  }

  addDefaultSrc(ev) {
    ev.target.src = missing
  }
  
  render () {
  	var photos = []

  	this.state.photos.map((photo) => {
      photos.push({original: photo, thumbnail: photo});
    });
    return (
      <React.Fragment>
	    <div>
			<ImageGallery items={photos} showFullscreenButton={false} onImageError={this.addDefaultSrc} onThumbnailError={this.addDefaultSrc} />
		</div>
      </React.Fragment>
    );
  }
}

export default PageGallery