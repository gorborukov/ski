class Hotel < ApplicationRecord
  after_create :add_external
 
  has_attached_file :picture, styles: { original: "1200x900>", large: "640x480#", medium: "320x240#", thumb: "100x100#" }, default_url: "/images/missing.png"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/

  self.per_page = 12

  def add_external
  	uri = URI(self.data_url)
	  response = Net::HTTP.get(uri)
	  data = JSON.parse(response, object_class: OpenStruct)
	  self.external = data
	  self.rating = self.external['table']['rating'].to_f rescue 0
	  self.stars = self.external['table']['stars'].to_i rescue 0
	  self.price = self.external['table']['price'].to_i rescue 0
	  self.rooms = get_rooms(self.external['table']['link'])
	  self.reviews = get_reviews(self.external['table']['link'])
	  self.services = get_services(self.external['table']['link'])
	  self.save
  end

  def get_rooms(link)
    doc = Nokogiri::HTML(open(link+"?prefer_site_type=tdot", "User-Agent" => "Chrome/15.0.854.0"), nil, "UTF-8")
    parsed_rooms = doc.css('table.roomstable').first
    parsed_rooms.search('thead', '.bed-types-wrapper','.bedroom_bed_type',  '.toggle-room-info', '.editDatesForm','.occ_no_dates','.rt_show_dates').remove
    rooms = parsed_rooms
    return rooms
  end

  def get_reviews(link)
    doc = Nokogiri::HTML(open(link+"?prefer_site_type=tdot", "User-Agent" => "Chrome/15.0.854.0"), nil, "UTF-8")
    parsed_reviews = doc.css('div.multi_reviews').first
    parsed_reviews.search('h3', '.request-more-reviews').remove
    reviews = parsed_reviews
    return reviews
  end

  def get_services(link)
    doc = Nokogiri::HTML(open(link+"?prefer_site_type=tdot", "User-Agent" => "Chrome/15.0.854.0"), nil, "UTF-8")
    return doc.css('div.facilities-checklist.dark-headings').first
  end

  def picture_remote_url=(url)
    self.picture = open(url)
  end

  def coordinates
    self.external['table']['coordinates']
  end

end