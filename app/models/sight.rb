class Sight < ApplicationRecord
	has_attached_file :picture, styles: { original: "1200x900>", main: "480x640#", large: "640x480#", medium: "320x240#", thumb: "100x100#" }, default_url: "/images/missing.png"
    validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/
end
